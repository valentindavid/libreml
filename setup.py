from setuptools import setup

setup(
    name = "auto_updater",
    version = "0.0.1",
    scripts = ['./auto_updater'],
    install_requires=[
        "python-gitlab>=1.12.1",
        "GitPython==3.0.4",
    ],
)
