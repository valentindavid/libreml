#!/usr/bin/env python3
# Auto-updates bst elements, puts each element in a feature branch
# and pushes it to gitlab
#
# Basic usage: ./auto_updater ELEMENT
# See ./auto_updater --help for all options

import argparse
import git
import gitlab
import os
import re
import subprocess


class BstElement:
    def __init__(self, element):
        self.element = element

    def __repr__(self):
        return "BstElement(element={0})".format(self.element)

    def _bst(self, *args, verbose=False, element=None):
        pipe = None
        if not verbose:
            pipe = subprocess.DEVNULL

        element_to_build = self.element
        if element:
            element_to_build = element
        # Terminate process if any input is required
        subprocess.run(
            ["bst", "--on-error=terminate", *args, element_to_build],
            check=True,
            stdout=pipe,
            stderr=pipe,
        )

    def build(self, verbose=False, element=None):
        try:
            self._bst("build", verbose=verbose, element=element)
            print("Built element {0}".format(self.element))
            return True
        except subprocess.CalledProcessError:
            print("Element {0} failed to build".format(self.element))
            return False

    def track(self, deps="all", verbose=False):
        self._bst("track", "--deps={0}".format(deps), verbose=verbose)


class BstRepo:
    def __init__(
        self, branch=None, ignore_elements=None, overwrite=False, verbose=False
    ):
        self.repo = git.Repo(".")
        active_branch = ""
        try:
            active_branch = self.repo.active_branch
        except:
            # We're in a detached HEAD, instead store the current sha
            active_branch = self.repo.head.object.hexsha

        if branch:
            self.base_branch = branch
        else:
            self.base_branch = active_branch

        self.original_branch = active_branch
        self.elements = self._get_updated_elements()
        if ignore_elements:
            self.elements = list(set(self.elements) - set(ignore_elements))
        self.bst_element_branches = []
        self.overwrite = overwrite

    def __enter__(self):
        self.create_branches()
        return self

    def __exit__(self, *args):
        self.repo.head.reset(index=True, working_tree=True)
        self.reset()

    def _get_updated_elements(self):
        return re.findall(r"elements\/.*", self.repo.git.status())

    def _get_commit_data(self, element, diff):
        new = re.search(r"\+\s+ref:\s+(.+)-0-g(.+)", diff)
        new_version_slug = new.group(1)
        new_sha = new.group(2)

        old = re.search(r"\-\s+ref:\s+(.+)-0-g(.+)", diff)
        old_version_slug = old.group(1)
        old_sha = old.group(2)

        commit_message = (
            "Update {0} to {1}\n\n".format(element, new_version_slug)
            + "Updates {0} to {1} ({2})\n".format(element, new_version_slug, new_sha)
            + "Previous version was {0} ({1})".format(old_version_slug, old_sha)
        )
        return commit_message

    def create_branches(self):
        repo = self.repo
        for e in self.elements:
            branch_name = "update/" + os.path.basename(e)
            self.bst_element_branches.append(
                (branch_name, BstElement(e.replace("elements/", "")))
            )
            try:
                branch = repo.create_head(branch_name, self.base_branch)
            except OSError:
                if self.overwrite:
                    print("deleting branch {0}".format(branch_name))
                    repo.delete_head(branch_name, "-D")
                    branch = repo.create_head(branch_name, self.base_branch)
                else:
                    print("Update branch {0} already created".format(branch_name))
                    continue
            self.checkout(branch)
            repo.index.add([e])
            diff = repo.git.diff("--cached")
            print("------{0}------".format(e))
            print(diff)
            msg = self._get_commit_data(element=e, diff=diff)
            repo.index.commit(msg)
            self._reset_to_base_branch()

        repo.head.reset(index=True, working_tree=True)
        return self.bst_element_branches

    def checkout(self, branch):
        self.repo.git.checkout(branch)

    # Return to the base branch that all update branches are based on
    # To be used internally by the class
    def _reset_to_base_branch(self):
        self.repo.git.checkout(self.base_branch)

    # Return to the branch before the updater was invoked
    def reset(self):
        self.repo.git.checkout(self.original_branch)

    def push_to_gitlab(
        self,
        branch,
        element,
        gitlab_project=None,
        gitlab_url="https://gitlab.com/",
        token_env="GITLAB_TOKEN",
        remote="origin",
        create_mr=False,
        force=False,
    ):
        self.checkout(branch)
        if not force:
            self.repo.git.push("--set-upstream", remote, branch)
        else:
            self.repo.git.push("-f --set-upstream", remote, branch)

        print("pushed branch {0}".format(branch))
        if create_mr:
            token = os.getenv(token_env)
            if not token:
                raise gitlab.exceptions.GitlabAuthenticationError(
                    "{0} env must be set to create MRs".format(token_env)
                )
            with gitlab.Gitlab(gitlab_url, token) as gl:
                if not gitlab_project:
                    print("gitlab_project must be specified to create MRs")
                else:
                    project = gl.projects.get(gitlab_project)
                    mr = project.mergerequests.create(
                        {
                            "source_branch": branch,
                            "target_branch": self.base_branch,
                            "title": "Update {0}".format(element.element),
                            "remove_source_branch": "true",
                        }
                    )
                    print("created MR for branch {0}".format(branch))


def parse_args():
    parser = argparse.ArgumentParser(
        description="""Auto-updates bst elements, puts each element in a feature branch 
            and pushes it to gitlab""",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "--verbose",
        default=False,
        action="store_true",
        help="Whether to display buildstream output",
    )
    parser.add_argument(
        "--overwrite",
        default=False,
        action="store_true",
        help="If branches already exist, overwrite them",
    )
    parser.add_argument(
        "--overwrite_remote",
        default=False,
        action="store_true",
        help="If remote branches already exist, force push",
    )
    parser.add_argument(
        "--nobuild",
        default=False,
        action="store_true",
        help="Disable building updated elements",
    )
    parser.add_argument(
        "--build_element",
        help="When testing updated elements, build the specified element instead",
    )
    parser.add_argument(
        "--bstignore",
        default=".bstignore",
        help="When updating bst elements, ignore the ones defined in bstignore",
    )
    parser.add_argument(
        "--base_branch",
        default="master",
        help="The base branch upon which update branches will branch off",
    )
    parser.add_argument(
        "--nodeps",
        default=False,
        action="store_true",
        help="Disable update of transitive dependencies",
    )
    parser.add_argument("--gitlab_project", help="The name of the gitlab project")
    parser.add_argument(
        "--push", default=False, action="store_true", help="Whether to push to gitlab"
    )
    parser.add_argument(
        "--create_mr",
        default=False,
        action="store_true",
        help="Whether to create an MR of created branches",
    )
    parser.add_argument("elements", nargs="+", help="The elements to update")

    return parser.parse_args()


def main():
    args = parse_args()
    if args.overwrite:
        print(
            "WARNING: --overwrite is enabled, existing update branches may be overwritten"
        )
    if args.overwrite_remote:
        print(
            "WARNING: --overwrite_remote is enabled, existing remote branches may be overwritten"
        )

    if git.Repo(".").is_dirty():
        print(
            "There are uncommitted changes, please commit before continuing the auto-updater"
        )
        exit(1)
    else:
        for element in args.elements:
            deps = "none"
            if not args.nodeps:
                deps = "all"
            BstElement(element).track(verbose=args.verbose, deps=deps)

            ignore_elements = []
            with open(args.bstignore) as f:
                def remove_comment(line):
                    i = line.find('#')
                    if i >= 0:
                        line = line[:i]
                    return line.strip()

                for e in f:
                    if not e.strip().startswith('#'):
                        ignore_elements.append("elements/" + remove_comment(e))

            with BstRepo(
                branch=args.base_branch,
                ignore_elements=ignore_elements,
                overwrite=args.overwrite,
            ) as repo:
                bst_element_branches = repo.bst_element_branches
                for branch, element in bst_element_branches:
                    repo.checkout(branch)
                    to_push = args.push
                    if not args.nobuild:
                        # If the builds fail, we don't want to push to gitlab
                        to_push = element.build(
                            verbose=args.verbose, element=args.build_element
                        )
                    if args.push and to_push:
                        try:
                            repo.push_to_gitlab(
                                branch,
                                element,
                                gitlab_project=args.gitlab_project,
                                create_mr=args.create_mr,
                                force=args.overwrite_remote,
                            )
                        except gitlab.exceptions.GitlabCreateError as e:
                            print(
                                "Failed to create MR for {0}: {1}".format(
                                    element.element, e
                                )
                            )
                        except git.exc.GitCommandError as e:
                            print(
                                "Failed to push branch for {0}: {1}".format(
                                    element.element, e
                                )
                            )


if __name__ == "__main__":
    main()
