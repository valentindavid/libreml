![libreML logo](https://gitlab.com/libreml/libreml/wikis/uploads/1322f3261cad1cba205ee3d7d0e225f7/small.png)

We provide ready-made docker containers that give a reproducible, open source SDK for machine learning, with all components built from source using [Buildstream](https://buildstream.build)

Currently libreML supplies:
- scikit-learn
- openCV
- pytorch
- jupyter
- matplotlib

Coming soon:
- [tensorflow](https://gitlab.com/libreml/libreml/issues/41)
- [GPU support](https://gitlab.com/libreml/libreml/issues/1)

To find out more about libreML and the motivation behind the project: [project-wiki](https://gitlab.com/libreml/libreml/wikis/home)

## Getting started

### How to use libreML?

Master branches and release tags will have associated docker containers published in
the [libreML container registry](https://gitlab.com/libreml/libreml/container_registry)

To run the docker containers you can use:

```shell
docker run -p 8888:8888 registry.gitlab.com/libreml/libreml/libreml:<TAG> jupyter-notebook --ip=0.0.0.0 --port=8888 --allow-root
```

This will present you with a local instance of Jupyter notebook!

### Want to help out?

If you want to add/fix/update any elements in the libreML stack, then you will need to do a little setup before you can get building.

You can find a full guide to setting up the development environment [here](https://gitlab.com/libreml/libreml/blob/master/CONTRIBUTING.md)

### Contact us?

If you are experiencing any issues, you can open an [issue](https://gitlab.com/libreml/libreml/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

If you want to contact us for any other reasons, you can find us on:

* IRC : #libreml on [freenode](https://webchat.freenode.net/)
* [Twitter](https://twitter.com/libreml)
